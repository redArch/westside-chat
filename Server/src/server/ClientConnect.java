package server;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class ClientConnect implements Runnable
{

	// create variables
	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
	private String color;
	private int position;
	private ArrayList<String> messages;
	private final int previousMsgCount = 50;

	public ClientConnect(Socket clientSocket, String color, ArrayList<String> messages, int position)
	{

		// constructor for client object
		this.messages = messages;
		this.color = color;
		this.clientSocket = clientSocket;
		this.position = position;

		out = null;
		in = null;

		// create input output streams
		try
		{
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}

	@Override
	public void run()
	{

		String userName = null;
		String userInput;

		try
		{
			// sends old messages
			if ((userInput = in.readLine()) != null)
			{
				for (int i = messages.size() - previousMsgCount; i < messages.size(); i++)
				{
					if (i >= 0)
					{
						sendOut(messages.get(i));
					}
				}
				// gets user screen name
				userName = userInput;
				// send out that the user has joined
				Server.addMsg(userName + " has joined the chat");
			}

			// sends out chat messages to each client
			while ((userInput = in.readLine()) != null)
			{
				if (!userInput.equals("") && userInput.length() <= 200)
				{
					Server.addMsg(userInput, userName, color);
				}
			}
		}
		catch (IOException e)
		{
			System.out.println(e);
		}

		try
		{
			in.close();
			out.close();
			clientSocket.close();
		}
		catch (IOException e)
		{

		}

		Server.removeClient(position, userName);
	}

	public void sendOut(String msg)
	{
		System.out.println(msg);
		Server.writeToFile(msg);
		out.println(msg);
		out.flush();
	}

	public void positionSetter(int pos)
	{
		position = pos;
	}

}
