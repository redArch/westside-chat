package server;

import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class Server
{

	// arryalist for each client
	private static final ArrayList<ClientConnect> clients = new ArrayList<>();
	// arraylist to track messages
	private static final ArrayList<String> messages = new ArrayList<>();
	// array of colours to be used as text colours (spelt as "colors" because that is how it is typically spelt in most languages)
	private static final String[] COLORS =
	{
		"#FF0000", "#00FF00", "#0000FF", "#FF0080", "#800080", "#FF00FF", "#FF8040", "#D4A017"
	};

	public static void main(String[] args)
	{
		// creates new server socket with value null
		ServerSocket serverSocket = null;

		// try catch for errors and exceptions
		try
		{
			// creates new serversocket on port 3456
			serverSocket = new ServerSocket(3456);
			// System.out.println("Created server socket.");
		}
		catch (IOException e)
		{
			System.out.println(e);
		}

		// test if server socket creation was successful
		if (serverSocket != null)
		{
			// get chat log in arraylist
			try
			{
				// log file
				File log = new File("log.txt");

				// check if it exists
				if (log.exists())
				{
					// new bufferedreader of log file
					BufferedReader br = new BufferedReader(new FileReader(log));
					String line = null;

					// loop through file line by line
					while ((line = br.readLine()) != null)
					{
						// keep arraylist under 100 to save ram
						if (messages.size() < 100)
						{
							messages.add(line);
						}
					}
					// close buffered reader
					br.close();
				}
			}
			catch (IOException e)
			{
			}
			// infinite loop
			while (true)
			{
				// try catch for errors and exceptions
				try
				{
					System.out.println("Client Connecting...");

					// creates a new socket for each client
					Socket clientSocket = serverSocket.accept();

					// adds a new client object using client socket, color from array colors and the current messages in the arraylist
					clients.add(new ClientConnect(clientSocket, COLORS[clients.size() - (int) (Math.floor(clients.size() / COLORS.length) * COLORS.length)], messages, clients.size()));

					// creates a new thread using the above created object and starts it
					(new Thread(clients.get(ALlast(clients)))).start();
					System.out.println("Client Connected");
				}
				catch (IOException | NullPointerException e)
				{
					System.out.println("Exception in client connect");
				}
			}
		}
		// if the server socket was not created the program outputs "serverSocket null" and stops running
		else
		{
			System.out.println("serverSocket null");
		}
	}

	public static int ALlast(ArrayList j)
	{
		// return the last index of an arraylist
		return j.size() - 1;
	}

	public static void addMsg(String msg)
	{
		// add message as server with blcak text
		messages.add("<div><span style=\"color:black; word-wrap: break-text; display:block;\">" + msg + "<br /></span></div>");

		clients.forEach((client) ->
		{
			// send the message to each client
			client.sendOut(messages.get(ALlast(messages)));
		});

	}

	public static void addMsg(String userMsg, String userName, String userColor)
	{
		// add message as user with colored text
		messages.add("<div>" + userName + ": " + "<span style=\"color: " + userColor + "; word-wrap: break-text; display:block;\">" + userMsg + "<br /></span></div>");

		clients.forEach((client) ->
		{
			// send to each client the id count and the message
			client.sendOut(messages.get(ALlast(messages)));
		});

	}

	public static void removeClient(int pos, String userName)
	{
		// remove client in arraylist
		clients.remove(pos);

		// set the position in each client object to account for the removed client
		for (int i = pos; i < clients.size(); i++)
		{
			clients.get(i).positionSetter(i);
		}

		// math to try and keep the colors equally distibuted in the chat {
		int position = pos - (int) (Math.floor(pos / COLORS.length) * COLORS.length);

		String hold = COLORS[position];

		for (int i = position; i < COLORS.length - 1; i++)
		{
			COLORS[i] = COLORS[i + 1];
		}

		COLORS[clients.size() - (int) (Math.floor(clients.size() / COLORS.length) * COLORS.length)] = hold;

		// send server message that user has left the chat
		addMsg(userName + " has left the chat");
	}

	public static void writeToFile(String msg)
	{
		// file log
		File log = new File("log.txt");

		try
		{
			// if it does not exist create it
			if (!log.exists())
			{
				log.createNewFile();
			}

			// buffered writer to write to file
			BufferedWriter bw = new BufferedWriter(new FileWriter(log, true));

			// write message and add line
			bw.write(msg);
			bw.newLine();

			// close buffered writer
			bw.close();
		}
		catch (IOException e)
		{
		}
	}

}
