package client;

import java.io.*;
import java.util.ArrayList;
import javax.swing.text.*;
import javax.swing.text.html.HTMLDocument;

public class GetChat implements Runnable
{

	// create class variables
	private final BufferedReader in;
	private final ArrayList<String> messages = new ArrayList<>();
	private final int previousMsgCount = 50;
	private HTMLDocument doc = null;

	public GetChat(BufferedReader in)
	{
		// constructor to take the in stream
		this.in = in;
	}

	@Override
	public void run()
	{
		// string userInput to be used in reading from server
		String userInput;

		int messageCount = 0;

		// jtextpane to html document to add and remove messages
		// with html elements and css styling
		doc = (HTMLDocument) Client.chatOut.getStyledDocument();

		// try catch for IOException
		try
		{
			// read input
			while ((userInput = in.readLine()) != null)
			{
				messages.add(userInput);

				// remove old message
				if (messages.size() > previousMsgCount)
				{
					// remove last message in arraylist
					messages.remove(0);
				}

				if (messageCount > 100)
				{
					// clear the 50 oldest messages every 100 messages
					clearOld();
					// reset messageCount to current amount
					messageCount = messages.size();
				}

				try
				{
					// insert the last message
					doc.insertBeforeEnd(doc.getDefaultRootElement(), userInput);
				}
				catch (BadLocationException ex)
				{
				}
				// move the jtextpane caret to the end
				// so it scrolls to the bottom
				Client.chatOut.setCaretPosition(doc.getLength());

				// add one to message count
				messageCount++;
			}

		}
		catch (IOException e)
		{
		}
	}

	public void clearOld()
	{
		// clear old messages

		String insert = "";

		// loop through arraylist to create string
		for (int i = 0; i < messages.size(); i++)
		{
			insert = insert.concat(messages.get(i));
		}

		// use string to replce current content in chat
		try
		{
			doc.setInnerHTML(doc.getDefaultRootElement(), insert);
		}
		catch (BadLocationException | IOException ex)
		{
		}
	}

}
